// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDFIxCrH5nDMPi2_msdf8FeEpjvyFns0lM",
    authDomain: "bookworms-hub.firebaseapp.com",
    databaseURL: "https://bookworms-hub.firebaseio.com",
    projectId: "bookworms-hub",
    storageBucket: "bookworms-hub.appspot.com",
    messagingSenderId: "564351093092"
  }
};
